<?php

namespace App\Repository;

use App\Entity\SitesIblock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SitesIblock|null find($id, $lockMode = null, $lockVersion = null)
 * @method SitesIblock|null findOneBy(array $criteria, array $orderBy = null)
 * @method SitesIblock[]    findAll()
 * @method SitesIblock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SitesIblockRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SitesIblock::class);
    }

//    /**
//     * @return SitesIblock[] Returns an array of SitesIblock objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SitesIblock
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
