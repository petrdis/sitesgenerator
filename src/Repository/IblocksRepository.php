<?php

namespace App\Repository;

use App\Entity\Iblocks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Iblocks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Iblocks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Iblocks[]    findAll()
 * @method Iblocks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IblocksRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Iblocks::class);
    }

//    /**
//     * @return Iblocks[] Returns an array of Iblocks objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Iblocks
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
