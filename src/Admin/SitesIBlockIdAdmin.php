<?php
/**
 * Created by PhpStorm.
 * User: petr
 * Date: 15.05.18
 * Time: 19:24
 */

namespace App\Admin;

use App\Entity\Position;
use PhpParser\Builder\Method;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SitesIBlockIdAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('site_id')
            ->add('iblock_id')
            ->add('position', ModelType::class, [
                'class' => Position::class,
                'property' => 'name',
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('site_id')
            ->add('iblock_id')
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('site_id')
            ->add('iblock_id');
    }
}