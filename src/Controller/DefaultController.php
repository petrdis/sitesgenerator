<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route(
     *     "/",
     *     name="default",
     *     host="{site}.{domain}",
     *     defaults={"domain"="%domain%"},
     *     requirements={"domain"="%domain%"}
     *     )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($site)
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
