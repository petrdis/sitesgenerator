<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SitesRepository")
 */
class Sites
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $meta_description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $meta_keywords;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $analitics;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enable;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Templates", inversedBy="sites", cascade = {"persist"})
     */
    private $templates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SitesIblock", mappedBy="site_id", cascade = {"persist"})
     */
    private $sitesIblocks;

    public function __construct()
    {
        $this->sitesIblocks = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(string $meta_keywords): self
    {
        $this->meta_keywords = $meta_keywords;

        return $this;
    }

    public function getAnalitics(): ?string
    {
        return $this->analitics;
    }

    public function setAnalitics(?string $analitics): self
    {
        $this->analitics = $analitics;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }

    public function getTemplates(): ?Templates
    {
        return $this->templates;
    }

    public function setTemplates(?Templates $templates): self
    {
        $this->templates = $templates;

        return $this;
    }

    public function __toString() {
        return (string) $this->name;
    }

    /**
     * @return Collection|SitesIblock[]
     */
    public function getSitesIblocks(): Collection
    {
        return $this->sitesIblocks;
    }

    public function addSitesIblock(SitesIblock $sitesIblock): self
    {
        if (!$this->sitesIblocks->contains($sitesIblock)) {
            $this->sitesIblocks[] = $sitesIblock;
            $sitesIblock->setSiteId($this);
        }

        return $this;
    }

    public function removeSitesIblock(SitesIblock $sitesIblock): self
    {
        if ($this->sitesIblocks->contains($sitesIblock)) {
            $this->sitesIblocks->removeElement($sitesIblock);
            // set the owning side to null (unless already changed)
            if ($sitesIblock->getSiteId() === $this) {
                $sitesIblock->setSiteId(null);
            }
        }

        return $this;
    }
}
