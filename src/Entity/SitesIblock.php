<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SitesIblockRepository")
 */
class SitesIblock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sites", inversedBy="sitesIblocks")
     */
    private $site_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Iblocks", inversedBy="sitesIblocks")
     */
    private $iblock_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Position", inversedBy="sitesIblocks")
     */
    private $position;

    public function getId()
    {
        return $this->id;
    }

    public function getSiteId(): ?Sites
    {
        return $this->site_id;
    }

    public function setSiteId(?Sites $site_id): self
    {
        $this->site_id = $site_id;

        return $this;
    }

    public function getIblockId(): ?Iblocks
    {
        return $this->iblock_id;
    }

    public function setIblockId(?Iblocks $iblock_id): self
    {
        $this->iblock_id = $iblock_id;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getPosition()->getName();
    }
}
