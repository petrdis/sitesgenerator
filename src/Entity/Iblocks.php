<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IblocksRepository")
 */
class Iblocks
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SitesIblock", mappedBy="iblock_id")
     */
    private $sitesIblocks;

    public function __construct()
    {
        $this->sitesIblocks = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * @return Collection|SitesIblock[]
     */
    public function getSitesIblocks(): Collection
    {
        return $this->sitesIblocks;
    }

    public function addSitesIblock(SitesIblock $sitesIblock): self
    {
        if (!$this->sitesIblocks->contains($sitesIblock)) {
            $this->sitesIblocks[] = $sitesIblock;
            $sitesIblock->setIblockId($this);
        }

        return $this;
    }

    public function removeSitesIblock(SitesIblock $sitesIblock): self
    {
        if ($this->sitesIblocks->contains($sitesIblock)) {
            $this->sitesIblocks->removeElement($sitesIblock);
            // set the owning side to null (unless already changed)
            if ($sitesIblock->getIblockId() === $this) {
                $sitesIblock->setIblockId(null);
            }
        }

        return $this;
    }
}
