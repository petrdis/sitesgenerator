<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PositionRepository")
 */
class Position
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Templates", inversedBy="positions")
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SitesIblock", mappedBy="position")
     */
    private $sitesIblocks;

    public function __construct()
    {
        $this->template = new ArrayCollection();
        $this->sitesIblocks = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Templates[]
     */
    public function getTemplate(): Collection
    {
        return $this->template;
    }

    public function addTemplate(Templates $template): self
    {
        if (!$this->template->contains($template)) {
            $this->template[] = $template;
        }

        return $this;
    }

    public function removeTemplate(Templates $template): self
    {
        if ($this->template->contains($template)) {
            $this->template->removeElement($template);
        }

        return $this;
    }

    /**
     * @return Collection|SitesIblock[]
     */
    public function getSitesIblocks(): Collection
    {
        return $this->sitesIblocks;
    }

    public function addSitesIblock(SitesIblock $sitesIblock): self
    {
        if (!$this->sitesIblocks->contains($sitesIblock)) {
            $this->sitesIblocks[] = $sitesIblock;
            $sitesIblock->setPosition($this);
        }

        return $this;
    }

    public function removeSitesIblock(SitesIblock $sitesIblock): self
    {
        if ($this->sitesIblocks->contains($sitesIblock)) {
            $this->sitesIblocks->removeElement($sitesIblock);
            // set the owning side to null (unless already changed)
            if ($sitesIblock->getPosition() === $this) {
                $sitesIblock->setPosition(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return (string) $this->name;
    }
}
